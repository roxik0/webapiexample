using Swashbuckle.Application;
using System.Web.Http;
namespace Name.API
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "Super Api Grupowego Chatu");
                //    var xmlFile = @"C:\Users\student\source\repos\WebApiExamples\GGServer\bin\Debug\GGServer.xml";
                //    c.IncludeXmlComments(xmlFile);
                })
                .EnableSwaggerUi(c =>
                {

                });
            
        }
    }
}