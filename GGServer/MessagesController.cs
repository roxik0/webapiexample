﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using GGServer.Models;

namespace GGServer
{
    [Authorize]
    public class MessagesController:ApiController
    {
        public static List<Message> Messages=new List<Message>()
        {
            new Message(){ Content = "HEj ziomki"},
            new Message(){ Content = "HEj Ale Fajny kod"},
        };
        /// <summary>
        /// Costam
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Message> Get()
        {
            return Messages;
        }

        public void Post(string api,Message msg)
        {
            if (PeopleController.users.Any(p=>p.ApiKey==api))
            {

                Messages.Add(msg);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
        }
    }
}
