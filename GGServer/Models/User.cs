﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGServer.Models
{
    public class User
    {
        public int Number { get; set; }
        public string NickName { get; set; }
        public string  ApiKey { get; set; }
    }
    public class Message
    {
        public string Sender { get; set; }
        public string Content { get; set; }
    }
}
