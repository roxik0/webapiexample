﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using GGServer.Models;

namespace GGServer
{
    public class PeopleController:ApiController
    {
        public static List<User> users=new List<User>();
       /// <summary>
       /// Ta metoda zwraca wszystkich zalogowanych użytkowników chatu
       /// </summary>
       /// <returns>Lista użytkowników</returns>
        public IEnumerable<User> Get()
        {
            return users;
        }

        public User Post(User user)
        {
            user.ApiKey = Guid.NewGuid().ToString().Replace("-","");
            users.Add(user);
            return user;
        }
    }
}
