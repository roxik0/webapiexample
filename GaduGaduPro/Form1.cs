﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GaduGaduPro.Models;
using GaduGaduPro.ServiceReference1;

namespace GaduGaduPro
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* Service1Client client=new Service1Client();
             var data= client.GetData(10);
             MessageBox.Show(data);
     */
            HttpClient client = new HttpClient();
            string baseAddress = "http://localhost:9000/";

            var response = client.GetAsync(baseAddress + "api/People").Result;

            MessageBox.Show(response.ToString());
            MessageBox.Show(response.Content.ReadAsStringAsync().Result);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var users=new WebApiClient().GetAllUsers();
            listBox1.Items.Clear();
            foreach (var item in users)
            {
                listBox1.Items.Add(item);
            }
            var messages=new WebApiClient().GetAllMessages();
            
            StringBuilder builder=new StringBuilder();
            foreach (var message in messages)
            {
                builder.AppendLine($"{message.Sender} : {message.Content}");
            }

            textBox3.Text = builder.ToString();
        }

        

        private void loginBt_Click(object sender, EventArgs e)
        {
           var user= new WebApiClient().CreateUser(tbName.Text);
            CurrentUser = user;
            timer1.Start();
        }

        public SuperUsers CurrentUser { get; set; }

        private void sendBt_Click(object sender, EventArgs e)
        {
            new WebApiClient().SendMessage(CurrentUser, tbMessage.Text);
        }
    }
}
