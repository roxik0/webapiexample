﻿namespace GaduGaduPro.Models
{
    public class SuperUsers
    {
        public int Number { get; set; }
        public string Nickname { get; set; }
        public string ApiKey { get; set; }

        public override string ToString()
        {
            return $"{Number} - {Nickname}";
        }
    }

    public class Message
    {
        public string Sender { get; set; }
        public string Content { get; set; }
    }
}
