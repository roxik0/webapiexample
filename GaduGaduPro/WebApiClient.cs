﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using GaduGaduPro.Models;
using Newtonsoft.Json;

namespace GaduGaduPro
{
    public class WebApiClient
    {
        string baseAddress = "http://localhost:9000/";

        public IEnumerable<SuperUsers> GetAllUsers()
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync(baseAddress + "api/People").Result;

            if (response.IsSuccessStatusCode)
            {
                var usersStr = response.Content.ReadAsStringAsync().Result;
                List<SuperUsers> users = JsonConvert.DeserializeObject<List<SuperUsers>>(usersStr);
                return users;

            }

            return new List<SuperUsers>();
        }
        public IEnumerable<Message> GetAllMessages()
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync(baseAddress + "api/Messages").Result;

            if (response.IsSuccessStatusCode)
            {
                var usersStr = response.Content.ReadAsStringAsync().Result;
                List<Message> users = JsonConvert.DeserializeObject<List<Message>>(usersStr);
                return users;

            }

            return new List<Message>();
        }
        public SuperUsers CreateUser(string userName)
        {
            HttpClient client = new HttpClient();

            string contentStr = JsonConvert.SerializeObject(new SuperUsers() { Nickname = userName });

            var content = new StringContent(contentStr, Encoding.UTF8, "application/json");
            var response = client.PostAsync(baseAddress + "api/People", content).Result;

            var user = JsonConvert.DeserializeObject<SuperUsers>(response.Content.ReadAsStringAsync().Result);
            return user;
        }


        public void SendMessage(SuperUsers currentUser, string contentText)
        {
            HttpClient client = new HttpClient();

            string contentStr = JsonConvert.SerializeObject(new Message() { Sender = currentUser.Nickname,Content = contentText});

            var content = new StringContent(contentStr, Encoding.UTF8, "application/json");
            var response = client.PostAsync(baseAddress + $"api/Messages?api={currentUser.ApiKey}", content).Result;

        }
    }
}